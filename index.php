<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP Challenge</title>
</head>
<body>
    <h1>OOP Challenge</h1>

    <?php
    require_once 'animal.php';
    require_once 'frog.php';
    require_once 'ape.php';

    // Membuat objek dari class Animal
    $sheep = new Animal("shaun");
    echo "<p>Name: " . $sheep->name . "</p>"; // "shaun"
    echo "<p>Legs: " . $sheep->legs . "</p>"; // 4
    echo "<p>Cold Blooded: " . $sheep->cold_blooded . "</p>"; // "no"

    // Membuat objek dari class Ape
    $sungokong = new Ape("kera sakti");
    echo "<p>Name: " . $sungokong->name . "</p>"; // "kera sakti"
    echo "<p>Legs: " . $sungokong->legs . "</p>"; // 2
    echo "<p>Cold Blooded: " . $sungokong->cold_blooded . "</p>"; // "no"
    echo "<p>Ape Yell: ";
    $sungokong->yell(); // "Auooo"
    echo "</p>";

    // Membuat objek dari class Frog
    $kodok = new Frog("buduk");
    echo "<p>Name: " . $kodok->name . "</p>"; // "buduk"
    echo "<p>Legs: " . $kodok->legs . "</p>"; // 4 (tetap 4 karena tidak ada perubahan di class Frog)
    echo "<p>Cold Blooded: " . $kodok->cold_blooded . "</p>"; // "no"
    echo "<p>Frog Jump: ";
    $kodok->jump(); // "hop hop"
    echo "</p>";
    ?>
</body>
</html>
